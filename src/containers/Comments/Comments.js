import './Comments.styl';

import React, { Component } from 'react';
import {
  CommentsForm,
  CommentsHeader,
  Comment
} from 'components';
import _cloneDeep from 'lodash/cloneDeep';

import { bindActionCreators }       from 'redux';
import { connect }                  from 'react-redux';

import {
  CommentActions,
} from 'actions';

@connect(state => ({
  commentData: state.commentsReducer
}), dispatch => ({
  actions: bindActionCreators(CommentActions, dispatch)
}))
export default class Comments extends Component {


  _sort() {

    const copyComment = _cloneDeep(this.props.commentData.comments);
    copyComment.sort(this.sortRevers);
    this.props.actions.sortComments(copyComment)
  }

  sortRevers(a, b) {
    return a.comment - b.comment;
  }

  render() {
    const {
      comments
    } = this.props.commentData;
    return (
      <div className="c-comments">
        <CommentsForm
          actions={this.props.actions}
          comments={comments}
        />
        <CommentsHeader />
        {
          comments && comments.map((item, index) => (
            <div key={index}>
              <Comment
                item={item}
                index={index}
                comments={comments}
                counter={this.props.actions.counter} />
            </div>
          ))
        }
        {
          comments.length > 1 &&
            <button
              className="btn btn-block"
              type="button"
              onClick={::this._sort}
            >
              Sort
            </button>
        }
      </div>
    );
  }
}
