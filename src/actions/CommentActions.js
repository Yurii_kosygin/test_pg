import * as actionTypes from '../constants/CommentsConstants';
import _cloneDeep from 'lodash/cloneDeep';

export function addComment(comment, comments) {
  const copyComment = _cloneDeep(comments);
  copyComment.unshift(comment);
  return dispatch => {
    dispatch({
      type: actionTypes.ADD_COMMENT,
      comments: copyComment
    });
  };
}

export function sortComments(comments) {
  const copyComment = _cloneDeep(comments);
  return dispatch => {
    dispatch({
      type: actionTypes.ADD_COMMENT,
      comments: copyComment
    });
  };
}

export function counter(direction, comments, index) {
  const copyComment = _cloneDeep(comments);
  copyComment[index].count = copyComment[index].count + direction;
  return dispatch => {
    dispatch({
      type: actionTypes.ADD_COMMENT,
      comments: copyComment
    });
  };
}