/* eslint-disable no-multi-spaces */

import { combineReducers } from 'redux';

import testReducer        from './testReducer';
import commentsReducer    from './commentsReducer';

const rootReducer = combineReducers({
  testReducer,
  commentsReducer
});

export default rootReducer;
