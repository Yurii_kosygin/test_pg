import * as actionTypes from '../constants/CommentsConstants';

const initialState = {
  comments: []
};

export default function commentReducer(state = initialState, action) {
  const {
    type,
    comments
  } = action;

  switch (type) {
    case actionTypes.ADD_COMMENT:
    case actionTypes.SORT_COMMENTS:
    case actionTypes.COUNTER_INCREASE:
    case actionTypes.COUNTER_INCREASE:
      return {
        ...state,
        comments
      };

    default:
      return state;
  }
}
