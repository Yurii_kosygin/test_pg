import './Comment.styl';

import React, { Component } from 'react';
import { Link } from 'react-router';
import Reply from './Reply/Reply';
import Type from 'prop-type';

const testReplay = [
  {
    name: 'User1',
    comment: `The only really bad position is when you go
            first and you don't have a natural selection or flash reincarnation. Even in these
            scenarios though, you can usually skirt away and clear the board.`,
    timeAgo: 10,
    owner: 'User3'
  },
  {
    name: 'User2',
    comment: `Can i add youin game to watch some replays?.`,
    timeAgo: 14,
    owner: 'User3'
  },
  {
    name: 'User3',
    comment: `Can i add youin game to watch some replays?`,
    timeAgo: 16,
    owner: 'User3'
  },
];

export default class Comment extends Component {
  static propTypes = {
  };

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };
  }

  showReplay() {
    this.setState({ isOpen: !this.state.isOpen })
  }

  render() {
    const {
      isOpen
    } = this.state;

    const {
      item,
      index,
      counter,
      comments
    } = this.props;

    return (
      <div className="c-comment">
        <div className={isOpen && 'comment-wrapper comment-wrapper-has-reply'}>
          <div className="comment-item">
            <div className="comment-header">
              <a href="#">
                <img
                  alt="User-name"
                  src="http://i.playground.ru/i/00/00/00/00/user/default/icon.50x50.png"
                  className="avatar-image size32"
                />
                {item.name}
              </a>
              <time className="comment-timestamp">12 min ago</time>
            </div>
            <div className="comment-body">
              {item.comment}
            </div>
            <div className="comment-actions">
              <div className="comment-voting">
                <button className="up" onClick={() => counter(1, comments, index)}></button>
                <div className="score">{item.count}</div>
                <button className="down" onClick={() => counter(-1, comments, index)}></button>
              </div>
              <button
                className="btn btn-xs btn-reply"
                onClick={::this.showReplay}
              >
                reply
              </button>
            </div>
          </div>
          {
            isOpen && testReplay.map((item, index) => (
              <Reply
                key={index}
                reply={item}
              />
            ))
          }
        </div>
      </div>
    );
  }
}
