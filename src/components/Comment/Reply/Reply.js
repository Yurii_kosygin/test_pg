import './Reply.styl';

import React, { Component } from 'react';
import { Link } from 'react-router';
import PropTypes from 'prop-type';


export default class Reply extends Component {

  render() {
    const {
      name,
      timeAgo,
      owner,
      comment
    } = this.props.reply;

    return (
      <div className="c-reply">
        <div className="comment-item">
          <div className="comment-header">
              <span className="comment-author">
                <a href="#">
                  <img
                    alt="User-name"
                     src="http://i.playground.ru/i/00/00/00/00/user/default/icon.20x20.png"
                     className="avatar-image size20"
                  />
                  {name}
                </a>
              </span>
            <time className="comment-timestamp">{timeAgo} min ago</time>
          </div>
          <div className="comment-body">
            <span className="reply-target">@{owner}</span> {comment}
          </div>
          <div className="comment-actions">
            <div className="comment-voting">
              <button className="up"></button>
              <div className="score">0</div>
              <button className="down"></button>
            </div>
            <button className="btn btn-xs btn-reply">reply</button>
          </div>
        </div>
      </div>
    );
  }
}
