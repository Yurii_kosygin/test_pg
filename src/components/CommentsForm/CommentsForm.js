import './CommentsForm.styl';

import React, { Component } from 'react';
import Type from 'prop-type';


export default class CommentsForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      commentText: ''
    };
  }

  _writeText(e) {
    this.setState({ commentText: e.target.value })
  }

  _submitComment() {
    if (this.state.commentText !== '') {
      this.props.actions.addComment({comment: this.state.commentText, name: 'MyNick', count: 0}, this.props.comments);
      this.setState({ commentText: '' })
    }
  }

  render() {
    const {
      commentText
    } = this.state;

    return (
      <div className="c-comments-form">
        <form>
          <div className="comment-entry">
            <div className="comment-entry-header">
              <img
                alt="MyNick"
                src="http://i.playground.ru/i/00/00/00/00/user/default/icon.50x50.png"
                className="avatar-image size32"
              />
              MyNick
            </div>
            <div className="form-group">
              <textarea
                className="form-control"
                name="text"
                placeholder="Put your shit here..."
                onChange={::this._writeText}
                value={commentText}
              />
            </div>
            <div className="comment-entry-footer">
              <button
                type="button"
                className="btn btn-default btn-sm btn-block"
                onClick={::this._submitComment}
              >
                Submit
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
