import './CommentsHeader.styl';

import React, { Component } from 'react';
import { Link } from 'react-router';
import Type from 'prop-type';


export default class CommentsHeader extends Component {
  static propTypes = {
  };

  render() {
    return (
      <div className="c-comments-header">
        <div className="pull-right">
          <Link href="#" className="text-muted">Best</Link> |&#160;
          <Link href="#" className="active">Newest</Link> |&#160;
          <Link href="#" className="text-muted">Oldest</Link>
        </div>
        <b>6 Comments</b>
      </div>
    );
  }
}
